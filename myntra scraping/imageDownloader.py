import pandas as pd
from urllib.request import urlretrieve


df = pd.read_csv('Myntra300Casual.csv')
dfUrl = df['URL']


path = 'images/'
for i, url in enumerate(dfUrl):
    fileName = 'img-{}.jpg'.format(i)
    fullPath = path + fileName
    urlretrieve(url, fullPath)
    print('Done-{}'.format(i))
