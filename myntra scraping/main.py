from bs4 import BeautifulSoup
import json
import requests
import pandas as pd


search = "shirt"
page = "p="
numberOfPages = 100
# url = "https://www.myntra.com/{}".format(search)
url = "https://www.myntra.com/festive?"
r = requests.get(url, headers={'User-Agent': 'Chrome/85.0.4183.83'})
soup = BeautifulSoup(r.text, "html5lib")
jsonData = soup.find_all('script')[10].text.strip()[14:]
Data = json.loads(jsonData)
imageURL = []
for i in range(0, 50):
    imageURL.append(Data['searchData']['results']['products'][i]['images'][0]['src'])

print("main done")

if numberOfPages > 0:
    for i in range(2, numberOfPages+1):
        pageURL = url + page + str(i)
        print(pageURL)
        r = requests.get(pageURL, headers={'User-Agent': 'Chrome/85.0.4183.83'})
        soup = BeautifulSoup(r.text, "html5lib")
        jsonData = soup.find_all('script')[10].text.strip()[14:]
        Data = json.loads(jsonData)
        for i in range(0, 50):
            imageURL.append(Data['searchData']['results']['products'][i]['images'][0]['src'])


dataFrame = pd.DataFrame(imageURL)
# dataFrame.to_csv('{}.csv'.format(search))
dataFrame.to_csv('Myntra100Festive.csv')